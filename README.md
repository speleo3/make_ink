## What do you need this for?

If you want to help the Inkscape project by testing feature branches (branches of the Inkscape code that have been created by a developer
to work on a specific feature), or if you would like to install multiple Inkscape versions in parallel on Linux, this script is for you.

When run on the command line with the name of an Inkscape branch, it will build a package of it that it installs into a separate
location for that branch. It will also create a start script file on your desktop, that needs to be run to start the Inkscape program
for that branch. This is necessary to make sure that the different Inkscape versions will not mess up each others' preferences and
will not disturb how extensions work.

The builds created by this script will not interfere with an Inkscape installation that has been installed via package management
(ppa or official repositories) or snap.

This means that you can continue to use your stable Inkscape version, and have other versions available for testing.

Before you use this script, make sure that you have at least 3 GB of free disk space (more is better).

## INSTRUCTIONS


### Step 1: Install required software:

To execute this script, you will need:
`libnotify`, `git`, `checkinstall` and all dependencies for building the main program (find an always up-to-date list
[here](https://gitlab.com/inkscape/inkscape-ci-docker/blob/master/Dockerfile)), and `cmake`.

As of 2018-11-7, this means that you need to execute this installation command on the command line (tested for Ubuntu 16.04, Ubuntu 17.10 and Ubuntu 18.04):

`sudo apt-get install git libnotify-bin checkinstall intltool pkg-config python-dev libtool ccache libart-2.0-dev libaspell-dev libboost-dev libcdr-dev libgc-dev libgdl-3-dev libglib2.0-dev libgnomevfs2-dev libgsl-dev libgtk-3-dev libgtkmm-3.0-dev libgtkspell-dev libgtkspell3-3-dev libjemalloc-dev liblcms2-dev libmagick++-dev libpango1.0-dev libpng-dev libpoppler-glib-dev libpoppler-private-dev libpopt-dev libpotrace-dev librevenge-dev libsigc++-2.0-dev libsoup2.4-dev libvisio-dev libwpg-dev libxml-parser-perl libxml2-dev libxslt1-dev libyaml-dev python-lxml zlib1g-dev cmake libgtkmm-2.4-dev gdb adwaita-icon-theme-full build-essential libdouble-conversion-dev`

This will install up to 650 Mb of software.

If you want to help test the new variable font features, you will also need a more recent pango. This is available already per default in Ubuntu Cosmic Cuttlefish (18.10).
For Ubuntu 18.04, you can install it like this:

```
sudo add-apt-repository ppa:marenhachmann/pango
sudo apt-get update
sudo apt-get install libpango1.0-dev
```

Note that this ppa is inofficial, and no guarantees are made as to whether it will break your system. It worked for me. With a little bit of testing, I couldn't find any issues, but that does not mean that there aren't any.

### Step 2: Get the source code:

#### For this script:
Download https://gitlab.com/Moini/make_ink/-/archive/master/make_ink-master.zip and unpack it.

#### For Inkscape:
The script will by default look for the Inkscape source code in your documents directory. To find out where that is type `xdg-user-dir DOCUMENTS`
on the command line. Either in that directory, or in another directory of your choice, clone the git repository for Inkscape:

```
cd /directory/of/choice
git clone --recurse-submodules https://gitlab.com/inkscape/inkscape.git
```

(this will create a new directory for your local copy of the Inkscape git repository, which will take very long and require ~2GB of disk space!)

### Step 3: Adapt the settings for this script:

In the directory of the make_inkscape script, there is a file called 'make_ink_config_template'. Copy this file to 'make_ink_config'. This is your settings file. If you like, you can adapt the paths used for compilation in that file. The comments explain what they will do. 

Default parameters will work okay, too, if you followed the instructions above.

The only really important thing is the location of the git clone you made in step 2. If you did not use your documents directory for it,
then you need to write the correct path at ~ line 13, by replacing the text inside the quotations of "$DOCUMENTS_DIR/inkscape".

### Step 4: Build Inkscape with the script:

Execute

```
cd /directory/where/script/is_saved
./make_inkscape
```

in a terminal to build and install the Inkscape master (main development) branch,

or optionally, run

```
cd /directory/where/script/is_saved
./make_inkscape cool_branch_name
```

to install another Inkscape version from another branch, which must be in the common Inkscape git repository.

To force a build, in case something went wrong with the previous one, you can add ´-f´ after indicating a branch name:

```
cd /directory/where/script/is_saved
./make_inkscape cool_branch_name -f
```

(this will rebuild the Inkscape branch 'cool_branch_name' from scratch)

The script will take some time to execute, and it will also use a lot of your CPU power.

Near the end of the process, it will ask you to enter your password once, to be allowed to install the freshly created package.

### Step 5: Start your fresh Inkscape version:

The script will place two new files in your desktop directory. You **must** use one of them to start the new Inkscape version (if you don't,
you risk messing up preferences and extensions and also testing the wrong version).

If your desktop environment allows files in your Desktop directory to appear on the desktop (on Unity, this does not seem to work,
but on Xfce, it does), just double-click one of the two new files on your desktop to execute. Else navigate to the folder and double-click the file, or run it via terminal.

The script also creates a menu entry that you can find in the 'Graphics' category, next to your normal Inkscape installation. Some menus don't update on their own (e.g. the Xfce applications menu). In that case, you can still start the script manually, or you need to reload your desktop environment (or plain reboot).

#### Step 5a: Start Inkscape for using / general testing

If you just want to play around or judge performance, use the one whose name does not start with 'Db' (for 'debug'). Debugging slows Inkscape down, so this one has a better performance.

#### Step 5b: Start Inkscape to gather crash information

One of the two desktop files has got its name starting with 'Db'. Use this starter if you want to get a gdb traceback from crashes for sharing with developers. You can find the debug output file in your /tmp directory, as `/tmp/inkscape_debug_trace_<Date>.txt`. Attach this file to bug reports for developers to investigate the crash.

### Go!

You can verify the Inkscape version in the 'Help > About' submenu.

And now: have fun playing around with the other versions :) (and please report your findings to the developers).

### Uninstall branch packages again

When a branch has been merged into the official Inkscape development branch, and you do not want and need it anymore, your local
build can be uninstalled (they tend to accumulate ;-)). To get a nice list of all the different Inkscape versions that are installed
on your system, do:

```
dpkg --get-selections | grep inkscape
```

To remove one of them, do:

```
sudo dpkg --remove package_name_from_the_list
```

Now you can also delete the corresponding start script, the corresponding preferences directory in ~/.config/, the build directory for the branch, and the desktop file in ~/.local/share/applications.
